﻿using System;

namespace Konfig.Expressions.Attributes
{

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class ExpressionPropertyAttribute : Attribute
    {
        public ExpressionPropertyAttribute()
        {
            
        }

        public ExpressionPropertyAttribute(string displayName)
        {
            DisplayName = displayName;
        }

        public string DisplayName { get; set; }
    }
}
