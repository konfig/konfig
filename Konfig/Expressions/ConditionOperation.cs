﻿using System;
using System.Reflection;
using Konfig.Expressions.Operations;
using Konfig.Expressions.Providers;
using Newtonsoft.Json;

namespace Konfig.Expressions
{
	public class ConditionOperation
	{
		[JsonProperty("colval")]
		public string Column { get; set; }

		[JsonProperty("coldisp")]
		public string ColumnDisplayName { get; set; }

		[JsonProperty("opval")]
		public string OperationId { get; set; }

		[JsonProperty("opdisp")]
		public string OperationDisplayName { get; set; }

		[JsonProperty("val")]
		public string Value { get; set; }

		[JsonIgnore]
		private PropertyInfo _property;

		[JsonIgnore]
		private ExpressionOperation _operation;

		[JsonIgnore]
		private bool _isInited;

		private void Init(IExpressionProvider provider)
		{
			if (_isInited)
				return;

			_property = provider.GetProperty(Column);
			_operation = provider.GetOperation(OperationId);
			_isInited = true;

			if (_property == null)
				throw new Exception($"Column name \'{Column}\' in expression is unknown");

			if (_operation == null)
				throw new Exception($"Operation \'{OperationId}\' in expression is unknown");
		}

		public IConditionResult Test(object target, IExpressionProvider provider)
		{
			Init(provider);

			if (_property == null || _operation == null)
				throw new Exception("expression failed");

			var currentValue = _property.GetValue(target, null);
			return new ConditionOperationEvaluationResult(
				this,
				currentValue,
				_operation.TypeCheckingTest(currentValue, Value)
			);
		}

		public override string ToString()
		{
			return $"{ColumnDisplayName} {OperationDisplayName} \'{Value}\'";
		}
	}
}
