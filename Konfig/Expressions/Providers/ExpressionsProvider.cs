﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Konfig.Expressions.Attributes;
using Konfig.Expressions.Operations;
using Konfig.Expressions.Operators;
using Konfig.Extensions;

namespace Konfig.Expressions.Providers
{
	public class ExpressionProvider : IExpressionProvider
	{
		public class ExpressionPropertyInfo
		{
			public PropertyInfo Property { get; set; }
			public bool Visible { get; set; }
			public string TypeName { get; set; }
			public string DisplayName { get; set; }
		}

		private readonly IExpressionNamesProvider _namesProvider;
		private readonly Dictionary<string, ExpressionOperator> _operators;
		private readonly Dictionary<string, ExpressionOperation> _operations;
		private readonly Dictionary<string, ExpressionPropertyInfo> _properties;
		private readonly Dictionary<Type, string> _supportedTypes;

		public ExpressionProvider(Type type, IExpressionNamesProvider namesProvider,
			IEnumerable<ExpressionOperation> operations,
			IEnumerable<ExpressionOperator> operators,
			Dictionary<Type, string> supportedTypes)
		{
			_supportedTypes = supportedTypes;
			_namesProvider = namesProvider;
			_operators = operators.ToDictionary(_namesProvider.GetOperatorName);
			_operations = operations.ToDictionary(_namesProvider.GetOperationName);
			_properties = GetSupportedProperties(type);
		}

		public Dictionary<string, ExpressionPropertyInfo> GetSupportedProperties(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			return type.GetProperties(BindingFlags.Instance | BindingFlags.Public)
				.Where(x => x.CanRead)
				.Select(property => new
				{
					SupportedType = _supportedTypes.FirstOrDefault(x => x.Key.IsAssignableFrom(property.PropertyType)),
					Property = property
				})
				.Where(x => x.SupportedType.Key != null)
				.ToDictionary(x => _namesProvider.GetPropertyName(x.Property),
					x => GetExpressionPropertyInfo(x.Property, x.SupportedType.Value));
		}

		private ExpressionPropertyInfo GetExpressionPropertyInfo(PropertyInfo property, string typeName)
		{
			var attribute =
				property.GetCustomAttributes(typeof(ExpressionPropertyAttribute), false).FirstOrDefault() as
				ExpressionPropertyAttribute;
			return new ExpressionPropertyInfo()
			{
				DisplayName = !string.IsNullOrWhiteSpace(attribute?.DisplayName) ? attribute.DisplayName : property.Name,
				Property = property,
				TypeName = typeName,
				Visible = attribute != null
			};
		}

		public PropertyInfo GetProperty(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			ExpressionPropertyInfo info;
			return _properties.TryGetValue(name, out info) ? info.Property : null;
		}

		public ExpressionOperation GetOperation(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			ExpressionOperation result;
			return _operations.TryGetValue(name, out result) ? result : null;
		}

		public ExpressionData GetExpressionData()
		{
			return new ExpressionData
			{
				Variables = _properties.Select((x) => new ExpressionInfo()
				{
					Id = x.Key,
					Type = x.Value.TypeName,
					DisplayName = x.Value.DisplayName,
					Visible = x.Value.Visible
				}).ToList(),
				Operators = _operators.Select(x => new ExpressionInfo()
				{
					Id = x.Key,
					DisplayName = x.Value.GetType().GetTypeDisplayName(),
					Visible = true,
					Type = string.Empty
				}).ToList(),

				Operations = _operations.Select(x => new ExpressionInfo()
				{
					Id = x.Key,
					DisplayName = x.Value.GetType().GetTypeDisplayName(),
					Type = x.Value.AcceptedTypeName,
					Visible = true
				})
			};
		}

		public ExpressionOperator GetOperator(string name)
		{
			if (string.IsNullOrWhiteSpace(name))
				throw new ArgumentNullException(nameof(name));

			ExpressionOperator result;
			return _operators.TryGetValue(name, out result) ? result : null;
		}
	}
}
