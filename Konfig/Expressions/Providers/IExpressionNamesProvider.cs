﻿using System.Reflection;
using Konfig.Expressions.Operations;
using Konfig.Expressions.Operators;

namespace Konfig.Expressions.Providers
{
    public interface IExpressionNamesProvider
    {
        string GetOperatorName(ExpressionOperator Operator);
        string GetOperationName(ExpressionOperation operation);
        string GetPropertyName(PropertyInfo property);
    }
}