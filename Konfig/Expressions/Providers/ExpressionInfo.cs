﻿namespace Konfig.Expressions.Providers
{
	public class ExpressionInfo
	{
		public string Id { get; set; }
		public string DisplayName { get; set; }
		public string Type { get; set; }
		public bool Visible { get; set; }
	}
}