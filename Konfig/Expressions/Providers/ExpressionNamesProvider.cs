﻿using System.Reflection;
using Konfig.Expressions.Operations;
using Konfig.Expressions.Operators;

namespace Konfig.Expressions.Providers
{
    public class ExpressionNamesProvider:IExpressionNamesProvider
    {
        public string GetOperatorName(ExpressionOperator Operator)
        {
            return Operator.GetType().FullName;
        }

        public string GetOperationName(ExpressionOperation operation)
        {
            return operation.GetType().FullName;
        }

        public string GetPropertyName(PropertyInfo property)
        {
            return property.Name;
        }
    }
}