using System.Collections.Generic;

namespace Konfig.Expressions.Providers
{
	public class ExpressionData
	{
		public IEnumerable<ExpressionInfo> Variables { get; set; }
		public IEnumerable<ExpressionInfo> Operators { get; set; }
		public IEnumerable<ExpressionInfo> Operations { get; set; }
	}
}