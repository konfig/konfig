﻿using System.Reflection;
using Konfig.Expressions.Operations;
using Konfig.Expressions.Operators;

namespace Konfig.Expressions.Providers
{
	public interface IExpressionProvider
	{
		ExpressionOperator GetOperator(string operatorName);
		PropertyInfo GetProperty(string propertyName);
		ExpressionOperation GetOperation(string operationName);
		ExpressionData GetExpressionData();
	}
}