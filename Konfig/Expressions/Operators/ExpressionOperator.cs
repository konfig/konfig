﻿using System.Collections.Generic;

namespace Konfig.Expressions.Operators
{
    public abstract class ExpressionOperator 
    {
        public abstract bool Test(IEnumerable<bool> values);
    }
}
