﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Konfig.Expressions.Operators
{

    ///<summary>
    ///</summary>
    [DisplayName("and")]
    public class AndOperator : ExpressionOperator
    {
        public override bool Test(IEnumerable<bool> values)
        {
            return values.All(value => value);
        }
    }
}
