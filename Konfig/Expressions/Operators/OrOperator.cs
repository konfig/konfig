﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Konfig.Expressions.Operators
{
    [DisplayName("or")]
    public class OrOperator : ExpressionOperator
    {
        public override bool Test(IEnumerable<bool> values)
        {
            return values.Any(value => value);
        }
    }
}
