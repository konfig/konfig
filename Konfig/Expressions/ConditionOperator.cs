﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Konfig.Expressions.Operators;
using Konfig.Expressions.Providers;
using Newtonsoft.Json;

namespace Konfig.Expressions
{
	public class ConditionOperator
	{
		[JsonProperty("operator")]
		public string Operator { get; set; }

		[JsonProperty("expressions")]
		public ConditionOperation[] Expressions { get; set; }

		[JsonProperty("opdisp")]
		public string OperatorDisplayName { get; set; }

		[JsonProperty("nestedexpressions")]
		public ConditionOperator[] NestedOperators { get; set; }

		[JsonIgnore]
		private ExpressionOperator _operator;

		[JsonIgnore]
		private bool _isInited;

		public IConditionResult Test(object target, IExpressionProvider provider)
		{
			if (target == null)
				throw new ArgumentNullException(nameof(target));
			if (provider == null)
				throw new ArgumentNullException(nameof(provider));

			Init(provider);

			var conditionResults = Enumerable.Empty<IConditionResult>();
			if (Expressions != null && Expressions.Length != 0)
				conditionResults = conditionResults.Concat(Expressions.Select(x => x.Test(target, provider)));

			if (NestedOperators != null && NestedOperators.Length != 0)
			{
				conditionResults = conditionResults.Concat(NestedOperators.Select(x => x.Test(target, provider)));
			}

			var evaluator = new LazyConditionsEvaluator(conditionResults);
			return new ConditionOperatorEvaluationResult(
				this,
				evaluator.EvaluatedConditionResults,
				_operator.Test(evaluator)
			);
		}

		private void Init(IExpressionProvider provider)
		{
			if (_isInited)
				return;

			_operator = provider.GetOperator(Operator);
			_isInited = true;
		}

		public override string ToString()
		{
			var op = $" {OperatorDisplayName} ";
			var lstExpressions = new List<string>();

			if (Expressions != null && Expressions.Length != 0)
				lstExpressions.AddRange(Expressions.Select(x => x.ToString()));

			if (NestedOperators != null && NestedOperators.Length != 0)
				lstExpressions.AddRange(NestedOperators.Select(x => x.ToString()));

			return $"( {string.Join(op, lstExpressions)} )";
		}

		#region CLASSES

		private class LazyConditionsEvaluator : IEnumerable<bool>
		{
			private readonly IEnumerable<IConditionResult> _lazyConditionResults;
			private readonly IList<IConditionResult> _evaluatedConditionResults;

			public LazyConditionsEvaluator(IEnumerable<IConditionResult> lazyConditionResults)
			{
				_lazyConditionResults = lazyConditionResults;
				_evaluatedConditionResults = new List<IConditionResult>();
			}

			public IEnumerator<bool> GetEnumerator()
			{
				foreach (var conditionResult in _lazyConditionResults)
				{
					_evaluatedConditionResults.Add(conditionResult);

					yield return conditionResult.Success;
				}
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return GetEnumerator();
			}

			public IEnumerable<IConditionResult> EvaluatedConditionResults => _evaluatedConditionResults;
		}

		#endregion
	}
}
