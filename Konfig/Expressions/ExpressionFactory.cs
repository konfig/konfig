﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Konfig.Expressions.Operations;
using Konfig.Expressions.Operators;
using Konfig.Expressions.Providers;
using Konfig.Utils;

namespace Konfig.Expressions
{
    public static class ExpressionFactory
    {
        private static readonly ReadOnlyCollection<ExpressionOperation> Operations;
        private static readonly ReadOnlyCollection<ExpressionOperator> Operators;
        private static readonly Dictionary<Type,string> SupportedTypes;
        
		static ExpressionFactory()
        {
            var operators = new List<ExpressionOperator>();
            var operations = new List<ExpressionOperation>();

            foreach (var type in  TypeUtils.GetCachedNonAbstractApplicationTypes())
            {
                if (typeof(ExpressionOperator).IsAssignableFrom(type))
                    operators.Add( (ExpressionOperator)Activator.CreateInstance(type));
                if ((typeof(ExpressionOperation).IsAssignableFrom(type)))
                    operations.Add((ExpressionOperation)Activator.CreateInstance(type));
            }

            Operators = operators.AsReadOnly();
            Operations = operations.AsReadOnly();
            SupportedTypes = new Dictionary<Type, string>();
	        foreach (var expressionOperation in operations)
	        {
		        SupportedTypes[expressionOperation.AcceptType] = expressionOperation.AcceptedTypeName;
	        }
        }

	    public static ExpressionProvider GetExpressionProvider(Type type, IExpressionNamesProvider provider = null)
	    {
		    return new ExpressionProvider(type, provider ?? new ExpressionNamesProvider(), Operations, Operators, SupportedTypes);
	    }
    }
}