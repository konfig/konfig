﻿namespace Konfig.Expressions
{
	public class ConditionResult : IConditionResult
	{
		internal const string FAILED_CONDITION_KEYWORD = "[!]";

		public ConditionResult(bool success, string evaluationTrace)
		{
			Success = success;
			EvaluationTrace = evaluationTrace;
		}

		public bool Success { get; }

		public string EvaluationTrace { get; }
	}
}
