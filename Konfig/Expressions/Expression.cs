﻿using System;
using Konfig.Expressions.Providers;
using Newtonsoft.Json;

namespace Konfig.Expressions
{
	public class Expression<T> : Expression
	{
		private static readonly IExpressionProvider Provider;

		static Expression()
		{
			Provider = ExpressionFactory.GetExpressionProvider(typeof(T));
		}

		public Expression(string json)
			: base(json, Provider)
		{
		}

		public bool? Evaluate(T target)
		{
			return base.Evaluate(target);
		}

		public IConditionResult EvaluateAndTrace(T target)
		{
			return base.EvaluateAndTrace(target);
		}
	}

	public class Expression
	{
		private readonly ConditionOperator _rootOperator;
		private readonly IExpressionProvider _provider;

		public readonly string Json;

		public Expression(string json, IExpressionProvider provider)
		{
			if (provider == null)
				throw new ArgumentNullException(nameof(provider));

			Json = json;
			_provider = provider;

			if (!string.IsNullOrWhiteSpace(json))
			{
				_rootOperator = JsonConvert.DeserializeObject<ConditionOperator>(json);
			}
		}

		public bool? Evaluate(object target)
		{
			return EvaluateAndTrace(target)?.Success;
		}

		public IConditionResult EvaluateAndTrace(object target)
		{
			return _rootOperator?.Test(target, _provider);
		}

		public override string ToString()
		{
			return _rootOperator?.ToString() ?? string.Empty;
		}
	}
}