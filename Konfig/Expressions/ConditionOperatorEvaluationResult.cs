﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Konfig.Expressions
{
	public class ConditionOperatorEvaluationResult : IConditionResult
	{
		private string _evaluationTrace;

		public ConditionOperatorEvaluationResult(ConditionOperator @operator, IEnumerable<IConditionResult> evaluatedConditionResults, bool success)
		{
			if (@operator == null)
				throw new ArgumentNullException(nameof(@operator));

			Operator = @operator;
			EvaluatedConditionResults = evaluatedConditionResults ?? Enumerable.Empty<IConditionResult>();
			Success = success;
		}

		private string BuildEvaluationTrace()
		{
			var joinedConditions = string.Join($" {Operator.OperatorDisplayName} ", EvaluatedConditionResults);
			var failedKeyword = Success ? string.Empty : ConditionResult.FAILED_CONDITION_KEYWORD;
			return $"{failedKeyword}({joinedConditions})";
		}

		public bool Success { get; }

		public IEnumerable<IConditionResult> EvaluatedConditionResults { get; }

		public ConditionOperator Operator { get; }

		public string EvaluationTrace
		{
			get
			{
				if (_evaluationTrace == null)
				{
					_evaluationTrace = BuildEvaluationTrace();
				}

				return _evaluationTrace;
			}
		}

		public override string ToString()
		{
			return EvaluationTrace;
		}
	}
}
