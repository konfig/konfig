﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.DoubleOperations
{
	[DisplayName("≤")]
	public class LessOrEqualOperation : GreaterOperation
	{
		public override bool Test(double val1, string val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
