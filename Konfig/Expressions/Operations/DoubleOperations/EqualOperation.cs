﻿using System;
using System.ComponentModel;

namespace Konfig.Expressions.Operations.DoubleOperations
{
	[DisplayName("=")]
	public class EqualOperation : DoubleExpressionOperation
	{
		/// <summary>
		/// Tests int
		/// </summary>
		/// <param name="val1"></param>
		/// <param name="val2"></param>
		/// <returns></returns>
		public override bool Test(double val1, string val2)
		{
			return Math.Abs(val1 - ConvertValueToAcceptedType(val2)) < Double.Epsilon;
		}
	}
}
