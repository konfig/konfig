﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.DoubleOperations
{
	[DisplayName("<")]
	public class LessOperation : DoubleExpressionOperation
	{
		public override bool Test(double val1, string val2)
		{
			return val1 < ConvertValueToAcceptedType(val2);
		}
	}
}
