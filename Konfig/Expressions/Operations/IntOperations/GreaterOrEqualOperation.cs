﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.IntOperations
{
	[DisplayName("≥")]
	public class GreaterOrEqualOperation : LessOperation
	{
		/// <summary>
		/// Tests int
		/// </summary>
		/// <param name="val1"></param>
		/// <param name="val2"></param>
		/// <returns></returns>
		public override bool Test(int val1, string val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
