﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.IntOperations
{
	[DisplayName("≤")]
	public class LessOrEqualOperation : GreaterOperation
	{
		public override bool Test(int val1, string val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
