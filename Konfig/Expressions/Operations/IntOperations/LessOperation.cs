﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.IntOperations
{
	[DisplayName("<")]
	public class LessOperation : IntExpressionOperation
	{
		public override bool Test(int val1, string val2)
		{
			return val1 < ConvertValueToAcceptedType(val2);
		}
	}
}
