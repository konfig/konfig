﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.IntOperations
{
	[DisplayName("=")]
	public class EqualOperation : IntExpressionOperation
	{
		/// <summary>
		/// Tests int
		/// </summary>
		/// <param name="val1"></param>
		/// <param name="val2"></param>
		/// <returns></returns>
		public override bool Test(int val1, string val2)
		{
			return val1 == ConvertValueToAcceptedType(val2);
		}
	}
}
