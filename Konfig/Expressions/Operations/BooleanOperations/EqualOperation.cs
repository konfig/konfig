﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.BooleanOperations
{

	[DisplayName("=")]
	public class EqualOperation : BoolExpressionOperation
	{
		public override bool Test(bool val1, string val2)
		{
			return val1 == ConvertValueToAcceptedType(val2);
		}
	}
}
