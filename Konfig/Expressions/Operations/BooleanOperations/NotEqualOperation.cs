﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.BooleanOperations
{

	[DisplayName("≠")]
	public class NotEqualOperation : EqualOperation
	{
		public override bool Test(bool val1, string val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
