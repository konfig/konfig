﻿using System;

namespace Konfig.Expressions.Operations
{

    public abstract class EnumExpressionOperation : ExpressionOperation
    {
        public override Type AcceptType
        {
            get { return typeof(Enum); }
        }

        public override string AcceptedTypeName
        {
            get { return "enum"; }
        }

        protected abstract bool Test(Enum val1, Enum val2);

        public override bool TypeCheckingTest(object val1, string val2)
        {
            if (val1 == null)
                throw new ArgumentNullException("val1");

            var type = val1.GetType();
            var enumValue1 = (Enum) val1;
            Enum enumValue2;
            if (string.IsNullOrWhiteSpace(val2))
            {
                enumValue2 = (Enum) Activator.CreateInstance(type);
            }
            else
            {
                enumValue2 = (Enum) Enum.Parse(type, val2, true);
            }

            return Test(enumValue1, enumValue2);
        }
    }
}
