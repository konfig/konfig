﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.StringOperations
{
	[DisplayName("contains (∋)")]
	public class ContainsOperation : StringExpressionOperation
	{
		public override bool Test(string val1, string val2)
		{
			val1 = ConvertValueToAcceptedType(val1);
			val2 = ConvertValueToAcceptedType(val2);
			return val1.Contains(val2);
		}
	}
}
