﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.StringOperations
{
	[DisplayName("doesn't contain (∌) ")]
	public class NotContainsOperation : ContainsOperation
	{
		public override bool Test(string val1, string val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
