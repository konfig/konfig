﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.StringOperations
{
	[DisplayName("≠")]
	public class NotEqualOperation : EqualOperation
	{
		public override bool Test(string val1, string val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
