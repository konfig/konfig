﻿using System;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace Konfig.Expressions.Operations.StringOperations
{
	[DisplayName("matches regex")]
	public class RegexOperation : StringExpressionOperation
	{
		public override bool Test(string val1, string val2)
		{
			if (string.IsNullOrEmpty(val2))
			{
				throw new Exception("regexp cannot be null or empty");
			}
			return new Regex(val2, RegexOptions.IgnoreCase).IsMatch(val1 ?? string.Empty);
		}
	}
}
