﻿using System;

namespace Konfig.Expressions.Operations
{

	/// <summary>
	/// Базовый класс над операциями над значением типа Double. Выполняет сравнение со стройкой введеной в редакторе условий.
	/// </summary>
	public abstract class DoubleExpressionOperation : ExpressionOperation
	{
		public override Type AcceptType => typeof(double?);

		public override string AcceptedTypeName => "double";

		/// <summary>
		/// Тестируем val1 по какой либо логике и сраниваем с введенным значением val2.
		/// </summary>
		/// <param name="val1">Возвращается произвольной функцией или полем лида.</param>
		/// <param name="val2">Сраниваемое значение, введенное в редакторе выражений.</param>
		/// <returns>Результат сравнения</returns>
		public abstract bool Test(double val1, string val2);

		public override bool TypeCheckingTest(object val1, string val2)
		{
			return Test(val1 == null ? 0 : (double)val1, val2);
		}
		/// <summary>
		/// Конвертируем некоторое значение в поддерживаемый тип.
		/// </summary>
		/// <param name="val">значение</param>
		/// <returns>сконверитированное значение</returns>
		public static double ConvertValueToAcceptedType(string val)
		{
			return Double.Parse(val);
		}
	}
}
