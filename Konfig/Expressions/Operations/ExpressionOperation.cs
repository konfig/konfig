﻿using System;

namespace Konfig.Expressions.Operations
{
    /// <summary>
    /// Base Expression class. 
    /// </summary>
    public abstract class ExpressionOperation 
    {
        /// <summary>
        /// Enumerates supported types
        /// </summary>
        public abstract Type AcceptType { get; }


        /// <summary>
        /// Gets the name of the accepted type.
        /// </summary>
        /// <value>
        /// The name of the accepted type.
        /// </value>
        public abstract string AcceptedTypeName { get; }

        /// <summary>
        /// Untyped testing
        /// </summary>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <returns></returns>
        public abstract bool TypeCheckingTest(object val1, string val2);
    }
}
