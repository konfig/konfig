﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.DateTimeOperations
{
	[DisplayName("<")]
	public class LessOperation : DateTimeExpressionOperation
	{
		public override bool Test(System.DateTime val1, string val2)
		{
			return val1 < ConvertValueToAcceptedType(val2);
		}
	}
}
