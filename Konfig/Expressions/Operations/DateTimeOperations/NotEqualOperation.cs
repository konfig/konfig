﻿using System.ComponentModel;

namespace Konfig.Expressions.Operations.DateTimeOperations
{
    [DisplayName("≠")]
    public class NotEqualOperation : EqualOperation
    {
        public override bool Test(System.DateTime val1, string val2)
        {
            return !base.Test(val1, val2);
        }
    }
}
