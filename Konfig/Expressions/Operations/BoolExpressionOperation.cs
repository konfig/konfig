﻿using System;
using Konfig.Extensions;

namespace Konfig.Expressions.Operations
{
    public abstract class BoolExpressionOperation : ExpressionOperation
    {
        public override Type AcceptType => typeof(bool?);
	    public override string AcceptedTypeName => "bool";
        public abstract bool Test(bool val1, string val2);

        public override bool TypeCheckingTest(object val1, string val2)
        {
            return Test( val1 != null && (bool)val1, val2);
        }
        public static bool ConvertValueToAcceptedType(string val)
        {
            return val.BoolUnsafe();
        }
    }
}
