﻿using System;
using System.ComponentModel;

namespace Konfig.Expressions.Operations.EnumOperations
{
	[DisplayName("=")]
	public class EqualOperation : EnumExpressionOperation
	{
		protected override bool Test(Enum val1, Enum val2)
		{
			return Equals(val1, val2);
		}
	}
}
