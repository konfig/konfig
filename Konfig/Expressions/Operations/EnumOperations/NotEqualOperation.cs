﻿using System;
using System.ComponentModel;

namespace Konfig.Expressions.Operations.EnumOperations
{
	[DisplayName("≠")]
	public class NotEqualOperation : EqualOperation
	{
		protected override bool Test(Enum val1, Enum val2)
		{
			return !base.Test(val1, val2);
		}
	}
}
