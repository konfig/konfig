﻿using System;

namespace Konfig.Expressions.Operations
{

    public abstract class StringExpressionOperation : ExpressionOperation
    {
        public override Type AcceptType
        {
            get { return typeof(string); }
        }


        public override string AcceptedTypeName
        {
            get { return "string"; }
        }


        /// <summary>
        /// Тестируем val1 по какой либо логике и сраниваем с введенным значением val2.
        /// </summary>
        /// <param name="val1">Возвращается произвольной функцией или полем лида.</param>
        /// <param name="val2">Сраниваемое значение, введенное в редакторе выражений.</param>
        /// <returns>Результат сравнения</returns>
        public abstract bool Test(string val1, string val2);


        public override bool TypeCheckingTest(object val1, string val2)
        {
            return Test((string)val1,val2);
        }


        /// <summary>
        /// Конвертируем некоторое значение в поддерживаемый тип.
        /// </summary>
        /// <param name="val">значение</param>
        /// <returns>сконверитированное значение</returns>
        public static string ConvertValueToAcceptedType(string val)
        {
	        return val?.ToLower() ?? string.Empty;
        }
    }
}
