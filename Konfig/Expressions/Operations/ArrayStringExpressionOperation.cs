﻿using System;
using System.Collections.Generic;

namespace Konfig.Expressions.Operations
{
    public abstract class ArrayStringExpressionOperation : ExpressionOperation
    {
        public override Type AcceptType => typeof(IEnumerable<string>);
	    public override string AcceptedTypeName => "arrayofstrings";
        public abstract bool Test(IEnumerable<string> val1, string val2);
        public override bool TypeCheckingTest(object val1, string val2)
        {
            return Test((IEnumerable<string>)val1, val2);
        }
        public static string ConvertValueToAcceptedType(string val)
        {
            return val??"";
        }
    }
}
