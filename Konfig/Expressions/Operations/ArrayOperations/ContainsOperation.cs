﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Konfig.Expressions.Operations.ArrayOperations
{

    [DisplayName("contains (∋)")]
    public class ContainsOperation : ArrayStringExpressionOperation
    {
        public override bool Test(IEnumerable<string> val1, string val2)
        {
            return val1 != null &&
                   val1.Any(x => string.Equals(val2, x, StringComparison.CurrentCultureIgnoreCase));
        }
    }
}
