﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Konfig.Expressions.Operations.ArrayOperations
{

    [DisplayName("doesn't contain (∌)")]
    public class NotContainsOperation :ContainsOperation
    {
        public override bool Test(IEnumerable<string> val1, string val2)
        {
            return !base.Test(val1, val2);
        }
    }

}
