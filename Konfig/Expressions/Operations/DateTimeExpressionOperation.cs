﻿using System;

namespace Konfig.Expressions.Operations
{    /// <summary>
	 /// Базовый класс над операциями над значением типа Datetime. Выполняет сравнение со стройкой введеной в редакторе условий.
	 /// </summary>
	public abstract class DateTimeExpressionOperation : ExpressionOperation
	{
		public override Type AcceptType => typeof(DateTime?);

		public override string AcceptedTypeName => "datetime";


		/// <summary>
		/// Тестируем val1 по какой либо логике и сраниваем с введенным значением val2.
		/// </summary>
		/// <param name="val1">Возвращается произвольной функцией или полем лида.</param>
		/// <param name="val2">Сраниваемое значение, введенное в редакторе выражений.</param>
		/// <returns>Результат сравнения</returns>
		public abstract bool Test(DateTime val1, string val2);

		public override bool TypeCheckingTest(object val1, string val2)
		{
			return Test(val1 == null ? DateTime.MinValue : (DateTime)val1, val2);
		}
		/// <summary>
		/// Конвертируем некоторое значение в поддерживаемый тип.
		/// </summary>
		/// <param name="val">значение</param>
		/// <returns>сконверитированное значение</returns>
		public static DateTime ConvertValueToAcceptedType(string val)
		{
			return DateTime.Parse(val);
		}
	}
}
