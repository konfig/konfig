﻿namespace Konfig.Expressions
{
	public interface IConditionResult
	{
		bool Success { get; }

		string EvaluationTrace { get; }
	}
}
