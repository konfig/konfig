﻿using System;
using System.Collections;
using System.Linq;

namespace Konfig.Expressions
{
	public class ConditionOperationEvaluationResult : IConditionResult
	{
		private string _evaluationTrace;

		public ConditionOperationEvaluationResult(ConditionOperation operation, object currentValue, bool success)
		{
			if (operation == null)
				throw new ArgumentNullException(nameof(operation));

			Operation = operation;
			CurrentValue = currentValue;
			Success = success;
		}

		private string BuildEvaluationTrace()
		{
			var failedKeyword = Success ? string.Empty : ConditionResult.FAILED_CONDITION_KEYWORD;

			string currentValueString;
			var enumerable = CurrentValue as IEnumerable;
			if (enumerable != null && !(CurrentValue is string))
			{
				currentValueString = $"[{string.Join(",", enumerable.Cast<object>().Select(v => $"\'{v}\'"))}]";
			}
			else
			{
				currentValueString = $"\'{CurrentValue}\'";
			}

			return $"{Operation.ColumnDisplayName}:{currentValueString} {failedKeyword}{Operation.OperationDisplayName} \'{Operation.Value}\'";
		}

		public bool Success { get; }

		public object CurrentValue { get; }

		public ConditionOperation Operation { get; }

		public string EvaluationTrace
		{
			get
			{
				if (_evaluationTrace == null)
				{
					_evaluationTrace = BuildEvaluationTrace();
				}
				return _evaluationTrace;
			}
		}

		public override string ToString()
		{
			return EvaluationTrace;
		}
	}
}
