using System;
using System.ComponentModel;

namespace Konfig.Extensions
{
	internal static class TypeExtensions
	{
		public static string GetTypeDisplayName(this Type type)
		{
			var attr = type.GetCustomAttributes(typeof(DisplayNameAttribute), false);
			if (attr.Length == 0 || String.IsNullOrEmpty(((DisplayNameAttribute)attr[0]).DisplayName))
				return  type.Name;

			return ((DisplayNameAttribute)attr[0]).DisplayName;
		}
	}
}