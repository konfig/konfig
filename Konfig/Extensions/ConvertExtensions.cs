﻿using System;

namespace Konfig.Extensions
{
	internal static class ConvertExtensions
	{
		public static bool BoolUnsafe(this object obj)
		{
			var result = BoolNullable(obj);
			if (result == null)
				throw new Exception("can't convert to bool" + obj);
			return result.Value;
		}
		public static bool? BoolNullable(this object obj)
		{
			if (obj == null)
				return null;

			if (obj is bool)
				return (bool)obj;

			var objAsString = obj.ToString();
			if (objAsString.Length == 0)
				return null;
			var normalized = objAsString.Trim().ToLower();
			switch (normalized)
			{
				case "false":
				case "0":
				case "n":
				case "no":
				case "f":
				case "off":
					return false;
				case "true":
				case "1":
				case "y":
				case "yes":
				case "t":
				case "on":
				case "true,false":
					return true;
			}
			return null;
		}
	}
}