﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Konfig.Utils
{
	internal static class TypeUtils
	{
		private static readonly Type[] ApplicationTypes;
		private static readonly ConcurrentDictionary<string, Type> TypeCache = new ConcurrentDictionary<string, Type>();
		private static readonly Assembly[] Assemblies;
		private static readonly Type[] NonAbstractTypes;

		static TypeUtils()
		{
			Assemblies = GetAssemblies().ToArray();
			ApplicationTypes = Assemblies.SelectMany(x => x.GetTypes()).ToArray();
			NonAbstractTypes = ApplicationTypes.Where(x => !x.IsInterface && !x.IsAbstract).ToArray();
		}

		public static Type[] GetCachedApplicationTypes()
		{
			return ApplicationTypes;
		}

		public static Type[] GetCachedNonAbstractApplicationTypes()
		{
			return NonAbstractTypes;
		}


		public static Type GetTypeByFullName(string typeName)
		{
			if (string.IsNullOrWhiteSpace(typeName))
				return null;

			var result = TypeCache.GetOrAdd(typeName, x =>
			{
				var t = Type.GetType(typeName);
				if (t != null)
					return t;
				foreach (var y in Assemblies)
				{
					t = y.GetType(typeName);
					if (t != null)
						break;
				}
				return t;
			});
			return result;
		}


		public static IEnumerable<Assembly> GetAssemblies()
		{
			var set = new HashSet<string>();
			var stack = new Stack<Assembly>();

			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				stack.Push(assembly);
				set.Add(assembly.FullName);
			}

			while (stack.Count > 0)
			{
				var assembly = stack.Pop();

				yield return assembly;

				foreach (var reference in assembly.GetReferencedAssemblies())
				{
					if (set.Add(reference.FullName))
					{
						Assembly referencedAssembly;
						try
						{
							referencedAssembly = Assembly.Load(reference);
						}
						catch (Exception ex)
						{
							throw new ApplicationException($"Can't load referenced assembly '{reference.FullName}' for assembly '{assembly.FullName}'", ex);
						}

						if (referencedAssembly.FullName == reference.FullName || set.Add(referencedAssembly.FullName))
						{
							stack.Push(referencedAssembly);
						}
					}
				}
			}
		}

		public static IEnumerable<Type> GetGenericIEnumerables(Type type)
		{
			return type
				.GetInterfaces()
				.Where(t => t.IsGenericType && t.GetGenericTypeDefinition() == typeof(IEnumerable<>))
				.Select(t => t.GetGenericArguments()[0]);
		}
	}
}

