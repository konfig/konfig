﻿using System;
using Konfig.Expressions;
using NUnit.Framework;

namespace Konfig.Tests
{
	[TestFixture]
	public class ExpressionTests
	{
		[Test]
		public void EvaluateAndTrace_ConditionsEvaluatedLazy()
		{
			var context = new ConditionContext
			{
				Value1 = "string",
				Value2 = 5,
				Value3 = new[] {"1", "2", "3"}
			};
			var expression = new Expression<ConditionContext>(@"{
	""operator"":""Konfig.Expressions.Operators.AndOperator"",
	""opdisp"":""and"",
	""expressions"":[
		{
			""colval"":""Value1"",
			""coldisp"":""Value1"",
			""opval"":""Konfig.Expressions.Operations.StringOperations.EqualOperation"",
			""opdisp"":""="",""val"":""string""
		}],
	""nestedexpressions"":[{
		""operator"":""Konfig.Expressions.Operators.OrOperator"",
		""opdisp"":""or"",
		""expressions"":[{
				""colval"":""Value2"",
				""coldisp"":""Value2"",
				""opval"":""Konfig.Expressions.Operations.IntOperations.GreaterOperation"",
				""opdisp"":"">"",
				""val"":""10""
			},{
				""colval"":""Value3"",
				""coldisp"":""Value3"",
				""opval"":""Konfig.Expressions.Operations.ArrayOperations.ContainsOperation"",
				""opdisp"":""contains"",
				""val"":""5""
			}],
		""nestedexpressions"":[]
	}]
}");

			var result = expression.EvaluateAndTrace(context);
			Assert.IsFalse(result.Success);

			Console.WriteLine($"Expression: {expression}");
			Console.WriteLine($"Result: {result.EvaluationTrace}");
		}

		private class ConditionContext
		{
			public string Value1 { get; set; }

			public int Value2 { get; set; }

			public string[] Value3 { get; set; }
		}
	}
}
